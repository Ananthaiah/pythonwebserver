"""
This module contains the Flask application for our web server.
"""
from flask import Flask
application = Flask(__name__)

@application.route('/')
def hello():
    """
    Returns a simple greeting message.

    Returns:
        str: A greeting message.
    """
    return "Hello, World!"

if __name__ == '__main__':
    application.run(debug=True)
